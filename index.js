let figlet = require('figlet')

figlet('Hello world Figlet', function(err, data) {
    if (err) {console.log('Error'); return;}
    console.log(data)
}

);

const chalk = require('chalk')
console.log(chalk.blue('Hello world Chalk'));

figlet('Hello world with both', function(err, data) {
    if (err) {console.log('Error 2'); return;}
    console.log(chalk.blue(data))
}

);